
var tdCell = {
	props: {
		cell: Object,
		editMode: Boolean,
		selectedCell: Object
	},
	watch: {
		selectedCell: function() {
			if (this.selectedCell)
				if (this.selectedCell.id != this.cell.id)
					this.setColorPicker(false)
		}
	},
	template: `
		<td>
			<input 
				:style="{'background-color':groupColor}"
				@click="selectCell"
				type="text"
				v-model="cell.text"
				class="editMode">
			<div v-show="editMode" ref="colorPicker" class="colorPicker">
				<input type="color" v-model="groupColor">
			</div>
		</td>
		`,
	computed: {
		groupColor: {
			get: function() {
				if (this.cell.group)
				 return this.cell.group.color
				else
					return "transparent"
			},
			set: function(color) {
				if (this.cell.group) {
					this.cell.group.color = color
					this.$emit('set-selected-cell', null)
				}
			}
		}
	},
	methods: {
		selectCell: function(event) {
			if (!this.editMode) {
				// first selection
				if (!this.selectedCell) {
					Thaco.addCellToGroup(this.cell, this.cell.group || new Thaco.Group(),)
					this.setColorPicker(true)
					this.$emit('set-selected-cell', this.cell)
				}
				// remove a cell from a group
				else if (this.selectedCell.id == this.cell.id) {
					Thaco.addCellToGroup(this.cell, null)
					this.setColorPicker(false)
					this.$emit('set-selected-cell', null)
				}
				// link two cells
				else {
					console.log('selection d\'une 2e cellule', this.selectedCell)
					Thaco.addCellToGroup(this.cell, this.selectedCell.group)
					this.$emit('set-selected-cell', null)
				}
			}
		},
		setColorPicker: function(isDisplayed) {
			if (isDisplayed)
				this.$refs.colorPicker.style.display = 'inline'
			else
				this.$refs.colorPicker.style.display = 'none'
		}
	}
}

var tbodyRow = {
	props: {
		row: Object,
		activeTab: Number,
		editMode: Boolean,
		selectedCell: Object
	},
	template: `
		<tbody>
			<tr>
				<td rowspan="2">
					<input v-if="editMode" type=text v-model="row.text">
					<input v-else type=text v-model="row.text" readonly>
				</td>
				<td-cell 
					v-for="sec in row.sections" 
					:key="sec[activeTab].background.id" 
					:edit-mode="editMode"
					:selected-cell="selectedCell"
					@set-selected-cell="$emit('set-selected-cell', ...arguments)"
					:cell="sec[activeTab].background">
				</td-cell>
			</tr>
			<tr>
				<td-cell 
					v-for="sec in row.sections" 
					:key="sec[activeTab].objective.id" 
					:edit-mode="editMode"
					:selected-cell="selectedCell"
					@set-selected-cell="$emit('set-selected-cell', ...arguments)"
					:cell="sec[activeTab].objective">
				</td-cell>
			</tr>
		</tbody>`,
	components: {
		'td-cell': tdCell
	}
}

var tableCollectedData = {
	data: function() {
		return {
			idRow: 1
		}
	},
	props: {
		content: Array,
		type: Function,
		activeTab: Number,
		editMode: Boolean,
		selectedCell: Object
	},
	created: function() {
  	this.addRow()
  },
	template: `
	<table>
		<thead>
			<tr>
				<th class="tableTitle">{{ type.tableName }}</th>
				<th>Theme #{{activeTab+1}}</th>
				<th>Location #{{activeTab+1}}</th>
				<th>NPC #{{activeTab+1}}</th>
				<th>Item #{{activeTab+1}}</th>
			</tr>
		</thead>
		<tbody-row v-for="row in content"
			:key="row.id"
			:row="row"
			:edit-mode="editMode"
			:selected-cell="selectedCell"
			@set-selected-cell="$emit('set-selected-cell', ...arguments)"
			:activeTab="activeTab">
		</tbody-row>
		<tbody v-if="editMode">
			<tr>
				<td colspan="5" class="addrow" @click="addRow()">+</td>
			</tr>
		</tbody>
	</table>
	`,
	methods: {
		addRow: function() {
			let row = new this.type(`${this.type.name} #${this.idRow++}`)
	  	for (let s of row.sections) {
	  		// FIXME: 50 should be nbTabs etc...
	  		for (let i = 0; i < 50; i++) {
	  			s.addempty()
	  		}
	  	}
	  	this.content.push(row)
		},
	},
	components: {
		'tbody-row': tbodyRow
	}
}

var tabsUl = {
	props: {
		activeTab: Number,
		nbTabs: Number
	},
	computed: {
		tabs: function() {
			let tabs = Array()
			for (let i = 0; i < this.nbTabs; i++) {
				tabs.push(`Tab #${i+1}`)
			}
			return tabs
		}
	},
	template: `
		<ul>
			<li v-for="(tab, i) in tabs"
				@click="$emit('set-active-tab', i)"
				:class="(i==activeTab)?'active':''">
				{{ tab }}
			</li>
			<li @click="addTab">+</li>
		</ul>
	`,
	methods: {
		addTab: function() {
			this.$emit('add-tab')
			this.$emit('set-active-tab', this.nbTabs)
		}
	}
}

var divHowTo = {
	props: {
		editMode: Boolean
	},
	template: `
		<div id="howto">
			<button @click="$emit('toggle-mode')">Change mode</button>
			<span v-if="editMode">
				Edit mode: Fill the tables with your collected data
			</span>
			<span v-else>
				Link mode: 
				<ul>
					<li>Connect two cells: click on a cell, then on another.</li>
					<li>Remove a cell from a group: click two times on a cell.</li>
				</ul>
			</span>
		</div>
	`
}

var app = new Vue({
  el: '#app',
  data: {
  	Character: Thaco.Character,
  	Quest: Thaco.Quest,
  	characters: Array(),
  	quests: Array(),
  	activeTab: 0,
  	nbTabs: 3,
  	editMode: true,
  	selectedCell: null
  },
  components: {
  	'table-collected-data': tableCollectedData,
  	'tabs-ul': tabsUl,
  	'div-how-to': divHowTo
  },
  methods: {
  	setActiveTab: function(activeTab) {
  		this.activeTab = activeTab
  	},
  	setSelectedCell: function(cell, callback) {
  		this.selectedCell = cell
  		if (callback)
  			callback(cell)
  	}
  }
})

Vue.directive('focus', {
    inserted: function (el) {
        el.focus()
    }
})